const path = require("path");
const fs = require("fs");
const jimp = require("jimp");

const iconSize = 32;
const itemFolder = path.resolve(__dirname, "..", "static", "images", "items");
const machineFolder = path.resolve(__dirname, "..", "static", "images", "machines");
const staticIconsFolder = path.resolve(__dirname, "..", "static", "images", "icons");
const iconFolder = path.resolve(__dirname, "..", "dist", "images", "icons");

function makeIcons(folder) {
	// Get all the files in the image folder
	fs.readdirSync(folder).forEach((file) => {
		const srcPath = path.resolve(folder, file);
		const destPath = path.resolve(iconFolder, file);

		console.log(`Processing "${file}"`);

		// Resize the image and write it to the icon folder
		jimp.read(srcPath)
		.then((image) => {
			return image
			.resize(iconSize, iconSize)
			.writeAsync(destPath)
		})
		.catch(onError);
	});
}

function copyIcons(folder) {
	fs.readdirSync(folder).forEach((file) => {
		const srcPath = path.resolve(folder, file);
		const destPath = path.resolve(iconFolder, file);

		console.log(`Copying "${file}"`);

		fs.copyFile(srcPath, destPath, (err) => {
			if (err) {
				onError(err.message);
			}
		});
	});
}

fs.mkdirSync(iconFolder, { recursive: true });

makeIcons(itemFolder);
makeIcons(machineFolder);
copyIcons(staticIconsFolder);

function onError(message) {
	console.error(message);
}
