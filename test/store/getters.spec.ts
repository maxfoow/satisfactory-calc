import { expect } from "chai";
import * as getters from "../../src/store/getters";
import { IState, defaultState } from "../../src/store";
import { machines } from "../../src/data/machines";

describe("store/getters", () => {
	let state: IState;

	beforeEach("Setup State", () => {
		// Reset the state to a fresh copy before each test
		state = defaultState();
	});

	describe("isBeltEnabled", () => {
		let isBeltEnabled;

		beforeEach(() => {
			isBeltEnabled = getters.isBeltEnabled(state);
		});

		it("should read the state of a belt", () => {
			// Belts should be enabled by default
			expect(isBeltEnabled(0)).to.equal(true);
		});

		it("should read the state of a disabled belt", () => {
			expect(isBeltEnabled(1)).to.equal(true);
			state.disabledBelts.push(1);
			expect(isBeltEnabled(1)).to.equal(false);
		});
	});

	describe("isMachineEnabled", () => {
		let isMachineEnabled;

		beforeEach(() => {
			isMachineEnabled = getters.isMachineEnabled(state);
		});

		it("should read the state of a machine", () => {
			// Machines should be enabled by default
			expect(isMachineEnabled(0)).to.equal(true);
		});

		it("should read the state of a disabled machine", () => {
			expect(isMachineEnabled(1)).to.equal(true);
			state.disabledMachines.push(1);
			expect(isMachineEnabled(1)).to.equal(false);
		});
	});

	describe("enabledMachines", () => {
		it("should return all machines by default", () => {
			expect(getters.enabledMachines(state)).to.deep.equal(machines);
		});

		it("should not return disabled machines", () => {
			const machineCount = machines.length;
			state.disabledMachines.push(1);
			expect(getters.enabledMachines(state).length).to.equal(machineCount - 1);
		});
	});
});
