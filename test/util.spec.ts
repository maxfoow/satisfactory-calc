import { expect } from "chai";
import { round } from "../src/util";

describe("util", () => {
	describe("round", () => {
		it("should round down properly", () => {
			expect(round(3.14159, 2)).to.equal("3.14");
		});

		it("should round up properly", () => {
			expect(round(3.14159, 3)).to.equal("3.142");
		});

		it("should keep trailing zeros", () => {
			expect(round(5.42, 4, true)).to.equal("5.4200");
		});

		it("should remove trailing zeros", () => {
			expect(round(5.42, 4)).to.equal("5.42");
			expect(round(5.42, 4, false)).to.equal("5.42");
		});

		it("should work with no decimals", () => {
			expect(round(5.42, 0)).to.equal("5");
		});
	});
});
