export const RECIPE_TYPES = [
	"harvest",
	"miner",
	"smelter",
	"constructor",
	"assembler",
	"manufacturer",
	"foundry",
	"pump",
	"refinery"
];
