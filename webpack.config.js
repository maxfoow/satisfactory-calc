const path = require("path");
const webpack = require("webpack");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const StatsPlugin = require("stats-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const nodeExternals = require("webpack-node-externals");

module.exports = function(env = {}, argv) {
	return {
		mode: env.production ? "production" : "development",
		context: path.resolve(__dirname, "src"),
		entry: "./main.ts",
		output: {
			path: path.resolve(__dirname, "dist"),
			filename: env.production ? "js/[name]-[contenthash].js" : "js/[name].js",
			pathinfo: env.production
		},
		module: {
			rules: [
				{
					test: /\.tsx?$/,
					exclude: /node_modules/,
					use: [
						{
							loader: "ts-loader",
							options: {
								// Needed for <script lang="ts"> to work in *.vue files; see https://github.com/vuejs/vue-loader/issues/109
								appendTsSuffixTo: [ /\.vue$/ ]
							}
						}
					]
				},
				{
					test: /\.vue$/,
					loader: "vue-loader"
				},
				{
					test: /\.css$/,
					use: env.test ? ["null-loader"] : [
						env.production ? MiniCssExtractPlugin.loader : "style-loader",
						{
							loader: "css-loader",
							options: {
								url: false
							}
						},
					]
				},
				{
					test: /\.less$/,
					use: env.test ? ["null-loader"] : [
						env.production ? MiniCssExtractPlugin.loader : "vue-style-loader",
						{
							loader: "css-loader",
							options: {
								url: false
							}
						},
						"less-loader"
					]
				}
			]
		},
		resolve: {
			alias: {
				lodash$: "lodash-es"
			},
			extensions: [".vue", ".tsx", ".ts", ".jsx", ".js"]
		},
		externals: env.test ? [nodeExternals()] : [],
		performance: {},
		devtool: env.production ? "source-map" : (env.test ? "inline-cheap-module-source-map" : "eval-soure-map"),
		target: env.test ? "node" : "web",
		plugins: [
			new VueLoaderPlugin(),
			new MiniCssExtractPlugin({
				filename: env.production ? "css/[name]-[contenthash].css" : "css/[name].css"
			}),
			new HtmlWebpackPlugin({
				template: path.resolve(__dirname, "static", "index.html"),
				minify: env.production ? {
					collapseWhitespace: true,
					removeComments: true,
					removeRedundantAttributes: true,
					removeScriptTypeAttributes: true,
					removeStyleLinkTypeAttributes: true,
					useShortDoctype: true
				} : false
			}),
			...(env.production ? [new CompressionPlugin({}), new webpack.HashedModuleIdsPlugin()] : []),
			...(env.profile ? [new StatsPlugin("stats.json", {})] : [])
		],
		optimization: env.test ? {} : {
			runtimeChunk: "single",
			splitChunks: {
				cacheGroups: {
					vendor: {
						test: /[\\/]node_modules[\\/]/,
						name: "vendors",
						chunks: "initial"
					}
				}
			},
			minimizer: env.production ? [
				new TerserPlugin({
					cache: true,
					parallel: true,
					sourceMap: true
				}),
				new OptimizeCSSAssetsPlugin({})
			] : []
		},
		devServer: {
			contentBase: path.resolve(__dirname, "dist"),
			compress: true,
			port: 4321
		},
		profile: !!env.profile,
		parallelism: env.profile ? 1 : 100
	};
};
