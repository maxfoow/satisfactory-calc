import Vue from "vue";

import "./error";
import router from "./router";
import store from "./store";
import "./analytics";
import { round, roundUp } from "./util";

import VueSelect from "vue-select";
import App from "./components/App.vue";
import GameIcon from "./components/GameIcon.vue";
import Recipe from "./components/Recipe.vue";

import "./styles/default.less";

Vue.component("v-select", VueSelect);
Vue.component("game-icon", GameIcon);
Vue.component("Recipe", Recipe);

Vue.filter("round", round);
Vue.filter("roundUp", roundUp);

// This is disabled via Webpack
Vue.config.productionTip = false;

// tslint:disable-next-line:no-unused-expression
new Vue({
	router,

	el: "#app",
	render: (h) => h(App),

	store
});
