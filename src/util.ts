/**
 * Round a number to a specified number of decimal places, optionally keeping trailing zeros.
 * @param value The value to round.
 * @param accruacy The number of digits after the decimal place to keep.
 * @param keep True to keep trailing zeros.
 */
export function round(value: number, accruacy: number, keep = false) {
	const fixed = value.toFixed(accruacy);
	return keep ? fixed : (+fixed).toString(10);
}

export function roundUp(value: number) {
	return Math.ceil(value);
}
