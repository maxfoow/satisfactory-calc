export type RecipeType = "harvest" | "miner" | "smelter" | "constructor" | "assembler" | "manufacturer" | "foundry" | "pump" | "refinery";

export type NodePurity = "Pure" | "Normal" | "Impure";

export interface IRecipeComponent {
	itemId: number;
	quantity: number;
}

export interface IRecipe {
	id: number;
	type: RecipeType;
	baseRate: number;
	output: IRecipeComponent;
	inputs: IRecipeComponent[];
}

export interface IItem {
	id: number;
	name: string;
	description?: string;
	icon: string;
	stackSize?: number;
}

export interface IMachine {
	id: number;
	name: string;
	icon: string;
	type: RecipeType;
	powerUsage: number;
	rateMultiplier: number;
}

export interface IProductionStep {
	// What recipe is used
	recipeId: number;

	// Which machine is being used
	machineId: number;

	// How many machines are required
	// This is a float, not an integer
	quantity: number;

	// What steps are required to make this step?
	required?: IProductionStep[];

	// What level of the production chain are we?
	// The desired outputs are level 0
	level: number;
}

export interface IItemRate {
	itemId: number;
	rate: number;
}

export interface IBelt {
	id: number;
	name: string;
	rate: number;
	icon: string;
}
