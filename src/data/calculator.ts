import { some } from "lodash";
import { IProductionStep, IItemRate, RecipeType, IMachine, IRecipe } from "../models";
import { itemsById } from "./items";
import { hardDriveRecipes } from "./recipes";

export function findBestRecipe(itemId: number, enabledRecipes: IRecipe[]) {
	// Determine which recipes will result in the output we want
	let options = enabledRecipes.filter((recipe) => recipe.output.itemId === itemId);
	if (options.length === 0) {
		throw new Error(`Could not find a recipe to make "${itemsById[itemId] ? itemsById[itemId].name : `item #${itemId}`}"`);
	}

	// Prefer the hard drive recipes
	// We do this by removing non-hard drive recipes if any of the HD recipes are in the options list
	if (some(options, (option) => hardDriveRecipes.includes(option.id))) {
		options = options.filter((option) => hardDriveRecipes.includes(option.id));
	}

	// Find the one with the highest baseRate
	// The harvest options have a baseRate of 0 which means they will always be picked last
	return options.reduce((best, option) => (best.baseRate * best.output.quantity) >= (option.baseRate * option.output.quantity) ? best : option, options[0]);
}

export function findBestMachine(type: RecipeType, enabledMachines: IMachine[]) {
	// Determine which machines will work for our recipe
	const options = enabledMachines.filter((machine) => machine.type === type);
	if (options.length === 0) {
		throw new Error(`Could not find a machine for type "${type}"`);
	}

	// Find the machine with the highest rateMultiplier
	return options.reduce((best, option) => best.rateMultiplier >= option.rateMultiplier ? best : option, options[0]);
}

export function calculateMachinesNeeded(requiredRate: number, recipeRate: number, machineRateMul: number, quanityPerRecipe: number) {
	return requiredRate / (recipeRate * machineRateMul * quanityPerRecipe);
}

/**
 * Calculates the amount of items needed per minute for a recipe input or output.
 * @param machineQuantity The number of machines running this recipe.
 * @param baseRate The base rate of the recipe. This is how many iterations of this recipe happen per minute.
 * @param numberPerRecipe The number of items that are needed per iteration.
 * @param machineRateMul The rate multipler for that level of machine.
 */
export function calculateItemRate(machineQuantity: number, baseRate: number, numberPerRecipe: number, machineRateMul: number) {
	return machineQuantity * baseRate * machineRateMul * numberPerRecipe;
}

export function calculateSteps(requiredItems: IItemRate[], settings: { enabledMachines: IMachine[], enabledRecipes: IRecipe[] }, level: number = 0): IProductionStep[] {
	// Recursivly, calculate what items are needed to create the required items
	return requiredItems.map((requirement) => {
		// Find the best recipe for this requirement
		const recipe = findBestRecipe(requirement.itemId, settings.enabledRecipes);

		// Find the best machine for this recipie type
		const machine = findBestMachine(recipe.type, settings.enabledMachines);

		// How many of this machine do we need to fufil the required rate?
		const quantity = calculateMachinesNeeded(requirement.rate, recipe.baseRate, machine.rateMultiplier, recipe.output.quantity);

		// What requirements does this step have?
		// The amount of input required:
		// (# of recipe input) * (reciper iterations / m) * (# of machines)
		const childRequirements = recipe.inputs.map((input) => ({
			itemId: input.itemId,
			rate: input.quantity * recipe.baseRate * quantity
		}));

		return {
			recipeId: recipe.id,
			machineId: machine.id,
			quantity,
			required: calculateSteps(childRequirements, settings, level + 1),
			level
		};
	});
}
