import { graphlib } from "dagre-d3";
import { IProductionStep } from "../../models";
import { recipesById } from "../recipes";
import { machinesById } from "../machines";
import { itemsById } from "../items";
import { round } from "../../util";
import { createMachineNode } from "./util";
import { calculateItemRate } from "../calculator";

export function createNormalGraph(productionSteps: IProductionStep[]) {
	const g = new graphlib.Graph();

	// Setup the default labels
	g.setGraph({});
	g.setDefaultEdgeLabel(() => ({}));

	fillNormalGraph(g, productionSteps);

	return g;
}

function fillNormalGraph(graph: graphlib.Graph, productionSteps: IProductionStep[], parentKey?: string) {
	// All all the steps in then connect them to the parent (If it exists)
	for (let i = 0; i < productionSteps.length; ++i) {
		const step = productionSteps[i];

		const recipe = recipesById[step.recipeId];
		const machine = machinesById[step.machineId];
		const outputName = itemsById[recipe.output.itemId].name;

		// Create a final output if this is the first step in the tree
		let localParentKey = parentKey;
		if (!parentKey) {
			localParentKey = `${outputName} ${i}`;
			graph.setNode(localParentKey, {label: outputName, class: "final-output"});
		}

		const key = createMachineNode(graph, machine, itemsById[recipe.output.itemId], step.quantity);

		const itemOutputRate = calculateItemRate(step.quantity, recipe.baseRate, recipe.output.quantity, machine.rateMultiplier);

		if (localParentKey) {
			// We want the items to flow from this node to the parent (output)
			graph.setEdge(key, localParentKey, {label: `${round(itemOutputRate, 1)} ${outputName}/m`});
		}

		if (step.required) {
			fillNormalGraph(graph, step.required, key);
		}
	}
}
